package com.example.demo.Util;

import com.example.demo.Model.Lease;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class LeaseAnalyzerTest {
    @Test
    public void hasOverlappingDates() {
        Lease l0 = new Lease();
        Lease l1 = new Lease();

        l0.setFrom(new Date(2000, Calendar.FEBRUARY, 2));
        l0.setTo(new Date(2000, Calendar.FEBRUARY, 8));

        l1.setFrom(new Date(2000, Calendar.FEBRUARY, 3));
        l1.setTo(new Date(2000, Calendar.FEBRUARY, 5));

        assertTrue(LeaseAnalyzer.hasOverlappingDates(l0, l1));
    }
    @Test
    public void doesntHaveOverlappingDates() {
        Lease l0 = new Lease();
        Lease l1 = new Lease();

        l0.setFrom(new Date(2000, Calendar.FEBRUARY, 2));
        l0.setTo(new Date(2000, Calendar.FEBRUARY, 8));

        l1.setFrom(new Date(2000, Calendar.MARCH, 3));
        l1.setTo(new Date(2000, Calendar.MARCH, 5));

        assertFalse(LeaseAnalyzer.hasOverlappingDates(l0, l1));
    }
}
