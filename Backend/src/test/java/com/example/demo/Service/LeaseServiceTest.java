package com.example.demo.Service;

import com.example.demo.Model.*;
import com.example.demo.Repository.LeaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
public class LeaseServiceTest {
    @Mock
    private UserService userService;
    @Mock
    private EquipmentService equipmentService;
    @Mock
    private SubRoomService subRoomService;
    @Mock
    private LeaseRepository leaseRepository;


    @InjectMocks
    private LeaseService leaseService;

    private User user0 = new User(0, "Bob", "email.com", "465", "USER", "pass", new Date(5000, Calendar.MARCH, 2));
    private Equipment equipment0 = new Equipment(0, "the old chair", "chair", "4 legs", "Xr543Q7", null);
    private Lease lease0 = new Lease(0, new Date(2000, Calendar.JANUARY, 1), new Date(2000, Calendar.JANUARY, 2), "my chair", user0, new HashSet<>(), new HashSet<>(Collections.singletonList(equipment0)));

    private List<Lease> leaseDatabase           = Collections.singletonList(lease0);
    private List<User> userDatabase             = Collections.singletonList(user0);
    private List<Equipment> equipmentDatabase   = Collections.singletonList(equipment0);
    private List<SubRoom> subRoomDatabase       = new ArrayList<>();

    @Before
    public void setup() throws Exception {
        equipment0.setLeases(new HashSet<>(Collections.singletonList(lease0)));

        when(userService.get(0)).thenReturn(user0);
        when(userService.get()).thenReturn(userDatabase);

        when(equipmentService.get(0)).thenReturn(equipment0);
        when(equipmentService.get()).thenReturn(equipmentDatabase);

        when(subRoomService.get()).thenReturn(subRoomDatabase);

        when(leaseRepository.get(0)).thenReturn(lease0);
        when(leaseRepository.get()).thenReturn(leaseDatabase);
        when(leaseRepository.add(any(Lease.class))).thenReturn(true);
    }

    @Test
    public void addWithValidLease() throws Exception {
        Lease l = new Lease();
        l.setDescription("desc");
        l.setFrom(new Date(2, Calendar.MARCH, 2));
        l.setTo(new Date(2, Calendar.MARCH,3));
        l.setEquipment(new HashSet<>(Collections.singletonList(equipment0)));
        l.setSubRooms(new HashSet<>());
        l.setLeaser(user0);

        assertTrue(leaseService.add(l));
    }

    @Test
    public void addWithoutLeasingAnything() throws Exception {
        Lease l = new Lease();
        l.setDescription("desc");
        l.setFrom(new Date(2, Calendar.MARCH, 2));
        l.setTo(new Date(2, Calendar.MARCH,3));
        l.setEquipment(new HashSet<>());
        l.setSubRooms(new HashSet<>());
        l.setLeaser(user0);

        Exception exception = assertThrows (IllegalArgumentException.class, () -> {
            leaseService.add(l);
        });
        String message = exception.getMessage();
        assertTrue(message.contains("Invalid lease: must lease something"));
    }

    @Test
    public void addWithInvalidDate() throws Exception {
        Lease l = new Lease();
        l.setDescription("desc");
        l.setFrom(new Date(5, Calendar.MARCH, 2));
        l.setTo(new Date(2, Calendar.MARCH,2));
        l.setEquipment(new HashSet<>(Collections.singletonList(equipment0)));
        l.setSubRooms(new HashSet<>());
        l.setLeaser(user0);

        Exception exception = assertThrows (IllegalArgumentException.class, () -> {
            leaseService.add(l);
        });
        String message = exception.getMessage();
        assertTrue(message.contains("Invalid date"));
    }

    @Test
    public void addWithInvalidSubRooms() throws Exception {
        Lease l = new Lease();
        l.setDescription("desc");
        l.setFrom(new Date(2, Calendar.MARCH, 2));
        l.setTo(new Date(2, Calendar.MARCH,3));
        SubRoom invalidSubRoom = new SubRoom();
        invalidSubRoom.setId(4534534);
        l.setEquipment(new HashSet<>());
        l.setSubRooms(new HashSet<>(Collections.singletonList(invalidSubRoom)));
        l.setLeaser(user0);

        Exception exception = assertThrows (IllegalArgumentException.class, () -> {
            leaseService.add(l);
        });
        String message = exception.getMessage();
        assertTrue(message.contains("Invalid room_section"));
    }

    @Test
    public void addWithInvalidEquipment() throws Exception {
        Lease l = new Lease();
        l.setDescription("desc");
        l.setFrom(new Date(2, Calendar.MARCH, 2));
        l.setTo(new Date(2, Calendar.MARCH,3));
        Equipment invalidEquipment = new Equipment();
        invalidEquipment.setId(4534534);
        l.setEquipment(new HashSet<>(Collections.singletonList(invalidEquipment)));
        l.setSubRooms(new HashSet<>());
        l.setLeaser(user0);

        Exception exception = assertThrows (IllegalArgumentException.class, () -> {
            leaseService.add(l);
        });
        String message = exception.getMessage();
        assertTrue(message.contains("Invalid equipment"));
    }

    @Test
    public void addWithInvalidLeaser() throws Exception {
        Lease l = new Lease();
        l.setDescription("desc");
        l.setFrom(new Date(2, Calendar.MARCH, 2));
        l.setTo(new Date(2, Calendar.MARCH,3));
        l.setEquipment(new HashSet<>(Collections.singletonList(equipment0)));
        l.setSubRooms(new HashSet<>());
        User invalidUser = new User();
        invalidUser.setId(5367894);
        l.setLeaser(invalidUser);

        Exception exception = assertThrows (IllegalArgumentException.class, () -> {
            leaseService.add(l);
        });
        String message = exception.getMessage();
        assertTrue(message.contains("Invalid leaser"));
    }

    @Test
    public void addWithInsufficientData() throws Exception {
        Lease l = new Lease();

        Exception exception = assertThrows (IllegalArgumentException.class, () -> {
            leaseService.add(l);
        });
        String message = exception.getMessage();
        assertTrue(message.contains("Invalid leaser"));
        assertTrue(message.contains("Invalid from"));
        assertTrue(message.contains("Invalid to"));
    }
}
