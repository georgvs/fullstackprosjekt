package com.example.demo.Exception;

public class ConflictException extends Exception {
    public ConflictException(String msg){
        super(msg);
    }
}
