package com.example.demo.Security;

import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsProvider implements UserDetailsService {

    private static UserService userService = new UserService(new UserRepository());

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User u = null;
        try {
            u = userService.getByName(s);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (u == null) throw new UsernameNotFoundException(s + " not found");
        return new UserDetailsImp(u);
    }
}
