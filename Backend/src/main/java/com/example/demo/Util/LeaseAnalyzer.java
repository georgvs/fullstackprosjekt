package com.example.demo.Util;

import com.example.demo.Model.Lease;

public class LeaseAnalyzer {
    /**
     * O(1)
     * checks if given leases have overlapping lease periods
     * NB: assumes that Lease.from is before Lease.to in both leases
     * @param l0 lease 1
     * @param l1 lease 2
     * @return isOverlapping
     */
    public static boolean hasOverlappingDates(Lease l0, Lease l1) {
        return l0.getFrom().equals(l1.getFrom()) || _hasOverlappingDates(l0, l1) || _hasOverlappingDates(l1, l0);
    }

    /**
     * Subroutine
     * @param l0 lease 1
     * @param l1 lease 2
     * @return l0 has time node inside of l1's time period
     */
    private static boolean _hasOverlappingDates(Lease l0, Lease l1) {
        return
        (
        //true if: l1.from -> l0.from <- l1.to
            l0.getFrom().after  (l1.getFrom())
                &&
            l0.getFrom().before (l1.getTo())
        )||(
        //true if: l1.from -> l0.to <- l1.to
            l0.getTo().after    (l1.getFrom())
                &&
            l0.getTo().before   (l1.getTo())
        );
    }
}
