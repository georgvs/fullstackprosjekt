package com.example.demo.Util;


import com.example.demo.Model.Equipment;
import com.example.demo.Model.Lease;
import com.example.demo.Model.StatisticInfo;
import com.example.demo.Model.SubRoom;
import com.example.demo.Service.LeaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

//TODO: Add logging and error handling
//TODO: Improve this shitty code
public class LeaseStatistics {


    private LeaseService leaseService;
    List<Lease> leases;

    public LeaseStatistics(LeaseService service) throws Exception {
        leaseService = service;
        leases = leaseService.get();
    }

    /**
     * Calculates the amount of hours for a lease
     * @param lease the lease which is to be calculated
     * @return The amount of hours for which a thing is rented
     */
    private long calculateHours(Lease lease) {
       long time_difference = lease.getTo().getTime() - lease.getFrom().getTime();

       return (time_difference / (1000 * 60 * 60));
    }

    /**
     * A method to get all the leases between a specific timeframe
     * @param leases The leases to check
     * @param from starting date
     * @param to End date
     * @return A list of alle the leases in between the dates
     */
    private List<Lease> appropiateLeases(List<Lease> leases, Date from, Date to) {

        List<Lease> properLeases = new ArrayList<>();

        for (int i = 0; i < leases.size(); i++) {
            if (leases.get(i).getFrom().after(from) && leases.get(i).getTo().before(to))
                properLeases.add(leases.get(i));
        }

        return properLeases;
    }

    /**
     * Finds all the leases for a specific euipment
     * @param id of the equipment to find leases for
     * @param from date to find leases from
     * @param to End date
     * @return a list of the amount of hours each user has rented the equipment
     */
    public List<StatisticInfo> getStatisticsEquipment(int id, Date from, Date to) {

        List<Lease> specificLeases = appropiateLeases(leases, from, to);
        List<StatisticInfo> info = new ArrayList<>();

        for (int i = 0; i < specificLeases.size(); i++) {
            ArrayList<Equipment> equipmentList = new ArrayList<Equipment>(specificLeases.get(i).getEquipment());
            for (int j = 0; j < specificLeases.get(i).getEquipment().size(); j++) {
                if (equipmentList.get(j).getId() == id)
                    info.add(new StatisticInfo(specificLeases.get(i).getLeaser(), calculateHours(specificLeases.get(i))));
            }
        }
        return fixList(info);
    }

    /**
     * Adds togheter duplicate usesrs in the list
     * @param stats the list to fix
     * @return a list without duplicates
     */
    private List<StatisticInfo> fixList(List<StatisticInfo> stats) {
        for (int i = 0; i < stats.size(); i++) {
            for (int j = 0; j < stats.size(); j++) {
                if (stats.get(i).getUser().getId() == stats.get(j).getUser().getId() && i != j) {
                    stats.get(i).setHours(stats.get(i).getHours() + stats.get(j).getHours());
                    stats.remove(j);
                }
            }
        }
        return stats;
    }

    /**
     * Finds all the leases for a specific subroom
      * @param id of the subroom to check
     * @param from starting date
     * @param to end date
     * @return a list of the amount of hours each user has rented the subroom
     */
    public List<StatisticInfo> getStatisticsSubRoom(int id, Date from, Date to) {

        List<Lease> specificLeases = appropiateLeases(leases, from, to);
        List<StatisticInfo> info = new ArrayList<>();

        for (int i = 0; i < specificLeases.size(); i++) {
            ArrayList<SubRoom> subRooms = new ArrayList<SubRoom>(specificLeases.get(i).getSubRooms());
            for (int j = 0; j < specificLeases.get(i).getSubRooms().size(); j++) {
                if (subRooms.get(j).getId() == id)
                    info.add(new StatisticInfo(specificLeases.get(i).getLeaser(), calculateHours(specificLeases.get(i))));
            }
        }
        return fixList(info);
    }

}
