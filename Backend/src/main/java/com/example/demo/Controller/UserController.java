package com.example.demo.Controller;

import com.example.demo.Controller.Basic.BasicController;
import com.example.demo.Model.User;
import com.example.demo.Service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController extends BasicController<User, User> {
    private UserService userService;

    public UserController(UserService service) {
        super(service);
        this.userService = service;
    }

    @GetMapping("name/{name}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public User getByName(@PathVariable("name") String name) throws Exception {
        return userService.getByName(name);
    }
    @GetMapping("email/{email}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public User getByEmail(@PathVariable("email") String email) throws Exception {
        return userService.getByEmail(email);
    }
    @GetMapping("role/{role}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<User> getByRole(@PathVariable("role") String role) throws Exception {
        return userService.getByRole(role);
    }

    @GetMapping("loggedIn")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public User getCurrentlyLoggedIn(Principal principal) throws Exception {
        return userService.getByName(principal.getName());
    }

    @Override
    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable int id, @RequestBody User u, Authentication auth) throws Exception {
        if (auth == null || auth.getName() == null)
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't be null");

        User dbU = userService.get(id);
        if (u.getRole() == null) u.setRole(dbU.getRole());

        if (!super.isAdmin(auth)){
            if (!userService.getByName(auth.getName()).equals(userService.get(id)))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't alter other user");
            if (!u.getRole().equals("USER"))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't change role unless admin");
            if (u.getValidUntil() != null)
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't change validUntil unless admin");
        }
        if (u.getValidUntil() == null) u.setValidUntil(dbU.getValidUntil());

        super.update(id, u, auth);
    }
    /**
     * Endpoint which allows a user to log in.
     * @param auth The user who is logging in
     * @return The details of the user who just logged in.
     */
    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public User login(Authentication auth) throws Exception {
        return userService.getByName(auth.getName());
    }
    @PostMapping("/logout")
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpSession session) {
        session.invalidate();
    }
}
