package com.example.demo.Controller;

import com.example.demo.Controller.Basic.BasicController;
import com.example.demo.Model.Message;
import com.example.demo.Model.Web.SimpleMessage;
import com.example.demo.Service.MessageService;
import com.example.demo.Service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("message")
public class MessageController extends BasicController<Message, SimpleMessage> {
    private MessageService messageService;
    private UserService userService;

    public MessageController(MessageService service, UserService userService) {
        super(service);
        this.messageService = service;
        this.userService = userService;
    }


    @Override
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody SimpleMessage pm, Authentication auth) throws Exception {
        pm.setPosterId(userService.getByName(auth.getName()).getId());
        super.add(pm, auth);
    }
}
