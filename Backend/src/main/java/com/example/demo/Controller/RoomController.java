package com.example.demo.Controller;

import com.example.demo.Controller.Basic.BasicController;
import com.example.demo.Model.Room;
import com.example.demo.Model.SubRoom;
import com.example.demo.Service.RoomService;
import com.example.demo.Service.SubRoomService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("room")
public class RoomController extends BasicController<Room, Room> {
    private RoomService roomService;
    private SubRoomService subRoomService;

    public RoomController(RoomService service, SubRoomService subRoomService) {
        super(service);
        this.roomService = service;
        this.subRoomService = subRoomService;
    }

    @GetMapping("section")
    @ResponseStatus(HttpStatus.OK)
    public List<SubRoom> getSection() throws Exception {
        return subRoomService.get();
    }

    @GetMapping("{roomId}/section")
    @ResponseStatus(HttpStatus.OK)
    public Set<SubRoom> get(@PathVariable Integer roomId) throws Exception {
        return subRoomService.getByRoomId(roomId);
    }
    @GetMapping("{roomId}/section/{sectionId}")
    @ResponseStatus(HttpStatus.OK)
    public SubRoom get(@PathVariable Integer roomId, @PathVariable Integer sectionId) throws Exception {
        return subRoomService.getByRoomIdAndSectionId(roomId, sectionId);
    }

    @PostMapping("{roomId}/section")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@PathVariable Integer roomId, @RequestBody SubRoom sr) throws Exception {
        Room r = new Room();
        r.setId(roomId);
        sr.setRoom(r);
        if (!subRoomService.add(sr))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
    }

    @PutMapping("{roomId}/section/{sectionId}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable Integer roomId, @PathVariable Integer sectionId, @RequestBody SubRoom sr) throws Exception {
        Room r = new Room();
        r.setId(roomId);
        sr.setRoom(r);
        if (!subRoomService.update(sectionId, sr))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
    }

    @DeleteMapping("{roomId}/section/{sectionId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Integer roomId, @PathVariable Integer sectionId) throws Exception {
        if (!subRoomService.delete(sectionId))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
    }
}
