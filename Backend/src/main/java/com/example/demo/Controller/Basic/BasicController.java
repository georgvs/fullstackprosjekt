package com.example.demo.Controller.Basic;

import com.example.demo.Exception.ConflictException;
import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Web.SimpleType;
import com.example.demo.Service.Basic.BasicService;
import javassist.NotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @param <T> full type used for get (e.g. Lease)
 * @param <ST> simplified type used for post and update (e.g. PostLease) PS A class can be its own simpleType (e.g. User implements SimpleType<User>)
 */
@CrossOrigin(origins = "http://localhost:8080", allowCredentials = "true")
public class BasicController<T extends IDable, ST extends IDable & SimpleType<T>> {
    private BasicService<T> service;

    public BasicController(BasicService<T> service) {
        this.service = service;
    }

    protected boolean isAdmin(Authentication auth){
        return auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("DEBUG") || a.getAuthority().equals("ADMIN"));
    }

    private HttpEntity<Map<String, String>> toHttpEntity (Exception e){
        Map<String, String> m = new HashMap<>();
        m.put("error", e.getMessage());
        return new HttpEntity<Map<String, String>>(m);
    }
    @ExceptionHandler({ NotFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public HttpEntity<Map<String, String>> handleException(NotFoundException e) {
        return toHttpEntity(e);
    }
    @ExceptionHandler({ IllegalArgumentException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public HttpEntity<Map<String, String>> handleException(IllegalArgumentException e) {
        return toHttpEntity(e);
    }
    @ExceptionHandler({ ConflictException.class })
    @ResponseStatus(HttpStatus.CONFLICT)
    public HttpEntity<Map<String, String>> handleException(ConflictException e) {
        return toHttpEntity(e);
    }


    /** GET: **/

    @GetMapping("")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<T> get(Authentication auth) throws Exception {
        return service.get();
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public T get(@PathVariable int id, Authentication auth) throws Exception {
        T o = service.get(id);
        if (o == null)
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
        else
            return o;
    }

    /** POST: **/

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody ST o, Authentication auth) throws Exception {
        if (!service.add(o.getUnsimplified()))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
    }

    /** PUT: **/

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable int id, @RequestBody ST o, Authentication auth) throws Exception {
        if (!service.update(id, o.getUnsimplified()))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
    }

    /** DELETE: **/

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable int id, Authentication auth) throws Exception {
        if (!service.delete(id))
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unknown error");
    }
}