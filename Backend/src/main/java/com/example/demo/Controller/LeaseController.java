package com.example.demo.Controller;

import com.example.demo.Controller.Basic.BasicController;
import com.example.demo.Model.Lease;
import com.example.demo.Model.StatisticInfo;
import com.example.demo.Model.User;
import com.example.demo.Model.Web.SimpleLease;
import com.example.demo.Service.LeaseService;
import com.example.demo.Service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("lease")
public class LeaseController extends BasicController<Lease, SimpleLease> {
    private LeaseService leaseService;
    private UserService userService;

    public LeaseController(LeaseService service, UserService userService) {
        super(service);
        this.leaseService = service;
        this.userService = userService;
    }

    /**
     * only ADMIN and DEBUG can post leases on behalf of others
     * @param pl PostLease
     * @param auth authentication
     * @throws Exception from service layer
     */
    @Override
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody SimpleLease pl, Authentication auth) throws Exception {
        verifyRequest(null, pl, auth);

        super.add(pl, auth);
    }

    private void verifyRequest(User expectedLeaser, SimpleLease pl, Authentication auth) throws Exception {
        if (auth == null || auth.getName() == null)
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't be null");

        User dbU = userService.getByName(auth.getName());

        if (pl.getLeaserId() == null)
            pl.setLeaserId(dbU.getId());
        if (!super.isAdmin(auth)) {
            if (!dbU.getId().equals(pl.getLeaserId()))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't lease on behalf of others");
            if (expectedLeaser != null && !dbU.equals(expectedLeaser))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid authentication: can't alter other user's leases");
        }
    }

    @Override
    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable int id, @RequestBody SimpleLease pl, Authentication auth) throws Exception {
        verifyRequest(leaseService.get(id).getLeaser(), pl, auth);

        super.update(id, pl, auth);
    }

    //TODO: Add error handling

    @PostMapping("/equipment/{id}/stat")
    @ResponseStatus(HttpStatus.OK)
    public List<StatisticInfo> getEquipmentStats(@PathVariable("id") int id, @RequestBody Map<String, Date> date, Authentication authentication ) throws Exception {
        if (super.isAdmin(authentication))
            return leaseService.statisticsEquipmentBased(id, date.get("from"), date.get("to"));
        throw new Exception(String.valueOf(HttpStatus.UNAUTHORIZED));
    }

    //TODO: Add error handling

    @PostMapping("/room/{id}/stat")
    @ResponseStatus(HttpStatus.OK)
    public List<StatisticInfo> getRoomStats(@PathVariable("id") int id, @RequestBody Map<String, Date> date, Authentication authentication ) throws Exception {
        if (super.isAdmin(authentication))
         return leaseService.statisticsRoomBased(id, date.get("from"), date.get("to"));
        throw new Exception(String.valueOf(HttpStatus.UNAUTHORIZED));
    }
}
