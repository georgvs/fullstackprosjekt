package com.example.demo.Controller;

import com.example.demo.Controller.Basic.BasicController;
import com.example.demo.Model.Equipment;
import com.example.demo.Service.EquipmentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("equipment")
public class EquipmentController extends BasicController<Equipment, Equipment> {
    private EquipmentService equipmentService;

    public EquipmentController(EquipmentService service) {
        super(service);
        this.equipmentService = service;
    }
}
