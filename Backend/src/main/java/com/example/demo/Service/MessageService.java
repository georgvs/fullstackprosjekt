package com.example.demo.Service;

import com.example.demo.Model.Message;
import com.example.demo.Repository.MessageRepository;
import com.example.demo.Service.Basic.BasicService;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import java.util.Date;

@Service
public class MessageService extends BasicService<Message> {
    private MessageRepository messageRepository;
    private SubRoomService subRoomService;
    private UserService userService;

    public MessageService(MessageRepository repository, SubRoomService subRoomService, UserService userService) {
        super(repository);
        this.messageRepository = repository;
        this.subRoomService = subRoomService;
        this.userService = userService;
    }


    private void validateMessage(Message m) throws Exception {
        validateDataFields(m);

        String notFoundError = "";
        m.setSubRoom(subRoomService.get(m.getSubRoom().getId()));
        if (m.getSubRoom() == null)     notFoundError += "Invalid room_section: no section of given id\n";
        m.setPoster(userService.get(m.getPoster().getId()));
        if (m.getPoster() == null)      notFoundError += "Invalid poster: no poster of given id\n";
        if (!notFoundError.isEmpty())
            throw new NotFoundException(notFoundError.trim());
    }
    private void validateDataFields(Message message) throws IllegalArgumentException {
        String errors = "";

        if (message.getContent() == null)                   errors += "Invalid content: can't be null\n";
        else if (message.getContent().isEmpty())            errors += "Invalid content: can't be empty\n";
        if (message.getPoster() == null)                    errors += "Invalid poster: can't be null\n";
        else if(message.getPostTime().after(new Date()))    errors += "Invalid date: can't be after current time\n";
        if (message.getSubRoom() == null)                   errors += "invalid room_section: can't be null\n";

        if (!errors.isEmpty())
            throw new IllegalArgumentException(errors.trim());
    }


    @Override
    public boolean add(Message m) throws Exception {
        m.setId(null);
        m.setPostTime(new Date());

        validateMessage(m);

        return super.add(m);
     }

    @Override
    public boolean update(int id, Message m) throws Exception {
        Message dbM = get(id);
        if (dbM == null) throw new NotFoundException("Invalid id, no message has id: " + id);

        m.setId(id);

        validateMessage(m);

        return super.update(id, m);
    }
}
