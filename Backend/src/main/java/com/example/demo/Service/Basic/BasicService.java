package com.example.demo.Service.Basic;

import com.example.demo.Model.Interface.IDable;
import com.example.demo.Repository.Basic.BasicRepository;
import javassist.NotFoundException;
import java.util.List;

public class BasicService<T extends IDable> {
    private BasicRepository<T> repository;

    public BasicService(BasicRepository<T> repository){
        this.repository = repository;
    }

    public boolean add(T o) throws Exception {
        return repository.add(o);
    }

    public boolean delete(int id) throws Exception {
        T o = repository.get(id);
        if (o == null) throw new NotFoundException("No " + repository.getModelName() + " with id " + id);

        return repository.delete(o);
    }

    public List<T> get() throws Exception {
        return repository.get();
    }

    public T get(int id) throws Exception {
        return repository.get(id);
    }

    public boolean update(int id, T o) throws Exception {
        return repository.update(id, o);
    }
}
