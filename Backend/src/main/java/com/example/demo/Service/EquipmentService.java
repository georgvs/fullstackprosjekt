package com.example.demo.Service;

import com.example.demo.Exception.ConflictException;
import com.example.demo.Model.Equipment;
import com.example.demo.Repository.EquipmentRepository;
import com.example.demo.Service.Basic.BasicService;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentService extends BasicService<Equipment> {
    private EquipmentRepository equipmentRepository;

    public EquipmentService(EquipmentRepository repository) {
        super(repository);
        this.equipmentRepository = repository;
    }

    private void validateDataFields(Equipment e) throws IllegalArgumentException {
        String errors = "";
        if (e.getName() == null)            errors += "Invalid name: can't be null\n";
        if (e.getType() == null)            errors += "Invalid type: can't be null\n";
        if (e.getSerialNumber() == null)    errors += "Invalid serial_number: can't be null\n";
        if (e.getDescription() == null)     errors += "Invalid description: can't be null\n";
        if (!errors.isEmpty())
            throw new IllegalArgumentException(errors.trim());
    }

    private void validateEquipment (Equipment e) throws Exception {
        validateDataFields(e);
        List<Equipment> allEquipment = equipmentRepository.get();
        for (Equipment dbE : allEquipment)
            if (!dbE.equals(e) && dbE.getType().equals(e.getType()) && dbE.getSerialNumber().equals(e.getSerialNumber()))
                throw new ConflictException("Invalid serial_number: equipment of type " + e.getType() + " with serial number " + e.getSerialNumber() + " already exists");
    }

    @Override
    public boolean add(Equipment e) throws Exception {
        e.setLeases(null);
        e.setId(null);
        validateEquipment(e);

        return super.add(e);
    }

    @Override
    public boolean update(int id, Equipment e) throws Exception {
        if (get(id) == null) throw new NotFoundException("Invalid id: no equipment has id " + id);
        e.setLeases(null);
        e.setId(id);
        validateEquipment(e);

        return super.update(id, e);
    }
}
