package com.example.demo.Service;

import com.example.demo.Model.Room;
import com.example.demo.Model.SubRoom;
import com.example.demo.Repository.RoomRepository;
import com.example.demo.Service.Basic.BasicService;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService extends BasicService<Room> {
    private RoomRepository roomRepository;

    public RoomService(RoomRepository repository) {
        super(repository);
        this.roomRepository = repository;
    }

    private void validateDataFields(Room r) throws IllegalArgumentException {
        String errors = "";
        if (r.getMaxPeople() == null)   errors += "Invalid max_people: can't be null\n";
        else if (r.getMaxPeople() < 1)  errors += "Invalid max_people: must allow more than 0 people";
        if (r.getName() == null)        errors += "Invalid name: can't be null\n";
        if (!errors.isEmpty())
            throw new IllegalArgumentException(errors.trim());
    }

    private void validateRoom(Room r) throws Exception {
        validateDataFields(r);

        if (r.getSubRooms() == null) return;
        int sum = 0;
        for (SubRoom sr : r.getSubRooms()) sum += sr.getMaxPeople();
        if (r.getMaxPeople() < sum)
            throw new IllegalArgumentException("Invalid max_people: room max people can't be less than " + sum + " (sum of max_people in room's room_sections)");
    }
    
    @Override
    public boolean add(Room r) throws Exception {
        r.setId(null);
        r.setSubRooms(null);

        validateRoom(r);

        return super.add(r);
    }

    @Override
    public boolean update(int id, Room r) throws Exception {
        Room dbR = get(id);
        if (dbR == null) throw new NotFoundException("Invalid id: no room has id " + id);

        r.setId(id);
        r.setSubRooms(dbR.getSubRooms());

        validateRoom(r);

        return super.update(id, r);
    }
}
