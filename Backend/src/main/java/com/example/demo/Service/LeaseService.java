package com.example.demo.Service;

import com.example.demo.Exception.ConflictException;
import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Lease;
import com.example.demo.Model.StatisticInfo;
import com.example.demo.Repository.LeaseRepository;
import com.example.demo.Service.Basic.BasicService;
import com.example.demo.Util.LeaseAnalyzer;
import com.example.demo.Util.LeaseStatistics;

import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LeaseService extends BasicService<Lease> {
    private LeaseRepository leaseRepository;
    private UserService userService;
    private EquipmentService equipmentService;
    private SubRoomService subRoomService;

    public LeaseService(LeaseRepository repository, UserService userService, EquipmentService equipmentService, SubRoomService subRoomService) {
        super(repository);
        this.leaseRepository = repository;
        this.userService = userService;
        this.equipmentService = equipmentService;
        this.subRoomService = subRoomService;
    }

    /**
     * O(nS + nDB): nS = {@param set}.size, nDB = {@param dbLst}.size
     * O(nDB) if {@param dbLst} intersects all of {@param set}
     *
     * Ensures that every element in {@param set} exists in {@param dbLst} (equation solely based on {@param <_T>}.id)
     * @param set set to be verified
     * @param dbLst database list to verify against
     * @param typeName name of type ({@param <_T>}) for exception message
     * @param <_T> type in {@param set} and {@param dbLst}
     * @return database copies of intersect between {@param set} and {@param dbLst}
     * @throws IllegalArgumentException to indicate that n elements in {@param set} do not exist in {@param dbLst}
     */
    private <_T extends IDable> Set<_T> validateSetAgainstDatabase(Set<_T> set, List<_T> dbLst, String typeName) throws IllegalArgumentException {
        Set<_T> intersect = new HashSet<>();
        //O(nDB)
        for (_T dbO : dbLst)
            //O(1) for hash set
            if (set.contains(dbO))
                intersect.add(dbO);

        //elements of set were not in dbLst => throw exception
        if (intersect.size() != set.size()) {
            StringBuilder invalidElements = new StringBuilder();
            //decoding which id(s) were bad
            //O(nS)
            for (_T o : set)
                if (!intersect.contains(o))
                    invalidElements.append(o.getId()).append(' ');
            throw new IllegalArgumentException("Invalid " + typeName + ": id(s) [" + invalidElements.toString().trim() + "] don't exist");
        }

        return intersect;
    }

    /**
     * O(nEq + nDB): nDB = #equipment in database, nEq = {@param l}.equipment.size
     *
     * Verifies that all equipment in lease exists in database
     * and replaces {@param l}.equipment with corresponding db copies
     * @param l lease
     * @throws Exception to indicate invalid equipment ids
     */
    private void validateEquipment(Lease l) throws Exception {
        if (l.getEquipment() == null) return;

        l.setEquipment(
                validateSetAgainstDatabase(l.getEquipment(), equipmentService.get(), "equipment")
        );
    }

    /**
     * O(nSr + nDB): nDB = #subRooms in database, nSr = {@param l}.subRooms.size
     *
     * Verifies that all subRooms in lease exists in database
     * and replaces {@param l}.subRooms with corresponding db copies
     * @param l lease
     * @throws Exception to indicate invalid subRoom ids
     */
    private void validateSubRooms(Lease l) throws Exception {
        if (l.getSubRooms() == null) return;

        l.setSubRooms(
                validateSetAgainstDatabase(l.getSubRooms(), subRoomService.get(), "room_section")
        );
    }
    /**
     * Verifies that leaser exists in database
     * and sets {@param l}.leaser to db cpy based on given id
     * @param l lease
     * @throws Exception either from repo layer or to indicate invalid leaser
     */
    private void validateLeaser(Lease l) throws Exception {
        l.setLeaser(userService.get(l.getLeaser().getId()));
        if (l.getLeaser() == null)
            throw new IllegalArgumentException("Invalid leaser: no leaser of given id exists");
        if (l.getLeaser().getValidUntil().before(new Date()))
            throw new IllegalArgumentException("Invalid leaser: leaser " + l.getLeaser().getName() + " has expired " + l.getLeaser().getValidUntil());
        if (l.getLeaser().getValidUntil().before(l.getTo()))
            throw new IllegalArgumentException("Invalid leaser: leaser " + l.getLeaser().getName() + " will expire before end of lease");
    }
    /**
     * O(nDB(mean(nEq) + mean(nSR))): nDB = #leases in database, nEq = {@param l}.equipment.size or less, nSR = {@param l}.subRooms.size or less
     *
     * Verifies that lease doesn't overlap with other leases' time periods if they lease the same instance
     * @param l lease
     * @throws Exception either from repo layer or to indicate overlap with existing database lease(s)
     */
    private void checkForDatabaseTimeOverlapConflicts(Lease l) throws Exception {
        StringBuilder conflicts = new StringBuilder();
        List<Lease> allLeases = leaseRepository.get();
        for (Lease dbL : allLeases) {
            //no overlapping equipment or subRooms (O(nEq + nSR))
            if (dbL.equals(l) /* can't overlap itself */ ||
                    (Collections.disjoint(l.getEquipment(), dbL.getEquipment()) && Collections.disjoint(l.getSubRooms(), dbL.getSubRooms())))
                continue;
            //O(1)
            if (LeaseAnalyzer.hasOverlappingDates(l, dbL))
                conflicts.append("lease/").append(dbL.getId()).append('\n');
        }
        if (conflicts.length() > 0)
            throw new ConflictException("Invalid dates: lease time period overlapping with:\n" + conflicts.toString().trim());
    }

    private interface LeaseFuncRef { void f(Lease l) throws Exception; }
    /**
     * Collects exception messages (i.a. for accumulative error handling)
     * @param exceptionTypeC type of exception to catch (i.e. filter)
     * @param f func which takes a lease
     * @param l lease to go into func
     * @param <ExceptionType> type
     * @return message of any exceptions thrown by func
     * @throws Exception if {@param f}.f threw exception of type other than {@param <ExceptionType>}
     */
    private <ExceptionType extends Exception> String catchExceptionMessage(Class<ExceptionType> exceptionTypeC, LeaseFuncRef f, Lease l) throws Exception {
        try { f.f(l); }
        catch (Exception e) {
            if (exceptionTypeC.isInstance(e)) return e.getMessage();
            else throw e;
        }
        return "";
    }

    /**
     * Verifies that lease is compatible with database (e.g. doesn't overlap with preexisting leases)
     * NB: expects to be called after this.validateDataFields(Lease)
     * @param l lease
     * @throws Exception to indicate invalid lease request
     */
    private void validateRequest(Lease l) throws Exception {
        String ex = "";
        ex += catchExceptionMessage(IllegalArgumentException.class, this::validateLeaser, l) + '\n';
        ex += catchExceptionMessage(IllegalArgumentException.class, this::validateEquipment, l) + '\n';
        ex += catchExceptionMessage(IllegalArgumentException.class, this::validateSubRooms, l) + '\n';
        ex += catchExceptionMessage(ConflictException.class, this::checkForDatabaseTimeOverlapConflicts, l) + '\n';
        ex = ex.trim();
        if (!ex.isEmpty())
            throw new IllegalArgumentException(ex);
    }
    /**
     * Verifies that lease contains sufficient and logical data (without querying database)
     * @param l lease
     * @throws IllegalArgumentException to indicate lacking or incorrect data
     */
    private void validateDataFields(Lease l) throws IllegalArgumentException {
        String errors = "";
        if (l.getLeaser() == null)              errors += "Invalid leaser: leaser can't be null\n";
        else if (l.getLeaser().getId() == null) errors += "Invalid leaser: leaser must have id\n";
        if (l.getFrom() == null)                errors += "Invalid from date: from can't be null\n";
        if (l.getTo() == null)                  errors += "Invalid to date: to can't be null\n";
        if (l.getSubRooms() == null)            errors += "Invalid room_sections: can't be null\n";
        if (l.getEquipment() == null)           errors += "Invalid equipment: can't be null\n";
        if (l.getDescription() == null)         errors += "Invalid description: can't be null\n";
        if (!errors.isEmpty() || l.getTo() == null)
            throw new IllegalArgumentException(errors.trim());

        if (l.getEquipment().isEmpty() && l.getSubRooms().isEmpty())
            throw new IllegalArgumentException("Invalid lease: must lease something");

        if (l.getTo().before(l.getFrom()))
            throw new IllegalArgumentException("Invalid dates: lease end cannot be before its beginning");
        if (l.getFrom().equals(l.getTo()))
            throw new IllegalArgumentException("Invalid dates: lease beginning must differ from its end");
    }

    /**
     * Validates a lease before it can be added to database
     * @param l lease
     * @throws Exception to indicate invalid lease or repo layer error
     */
    private void validateLease(Lease l) throws Exception {
        validateDataFields(l);
        validateRequest(l);
    }

    @Override
    public boolean add(Lease l) throws Exception {
        l.setId(null);

        validateLease(l);

        return super.add(l);
    }

    @Override
    public boolean update(int id, Lease l) throws Exception {
        if (get(id) == null) throw new NotFoundException("Invalid id: no lease has id " + id);
        l.setId(id);

        validateLease(l);

        return super.update(id, l);
    }

    //TODO: add error handling

    public List<StatisticInfo> statisticsEquipmentBased(int id, Date from, Date to) throws Exception {

        if (to.before(from))
            throw new IllegalArgumentException("Invalid Dates");

        LeaseStatistics leaseStatistics = new LeaseStatistics(this);

        return leaseStatistics.getStatisticsEquipment(id, from, to);
    }

    //TODO: add error handling

    public List<StatisticInfo> statisticsRoomBased(int id, Date from, Date to) throws Exception {

        if (to.before(from))
            throw new IllegalArgumentException("Invalid Dates");

        LeaseStatistics leaseStatistics = new LeaseStatistics(this);

        return leaseStatistics.getStatisticsSubRoom(id, from, to);
    }
}
