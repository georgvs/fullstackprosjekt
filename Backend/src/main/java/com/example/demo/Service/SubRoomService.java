package com.example.demo.Service;

import com.example.demo.Model.Room;
import com.example.demo.Model.SubRoom;
import com.example.demo.Repository.SubRoomRepository;
import com.example.demo.Service.Basic.BasicService;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import java.util.Set;

@Service
public class SubRoomService extends BasicService<SubRoom> {
    private SubRoomRepository subRoomRepository;
    private RoomService roomService;

    public SubRoomService(SubRoomRepository repository, RoomService roomService) {
        super(repository);
        this.subRoomRepository = repository;
        this.roomService = roomService;
    }

    public Set<SubRoom> getByRoomId(int roomId) throws Exception {
        Room r = roomService.get(roomId);
        if (r == null) throw new NotFoundException("Invalid room_id: room doesn't exist");
        return r.getSubRooms();
    }
    public SubRoom getByRoomIdAndSectionId(int roomId, int sectionId) throws Exception {
        for (SubRoom sr : getByRoomId(roomId)) if (sr.getId() == sectionId) return sr;
        throw new NotFoundException("Invalid section_id: section doesn't exist");
    }

    private void validateDataFields(SubRoom sr) throws IllegalArgumentException {
        String errors = "";
        if (sr.getRoom() == null)               errors += "Invalid room: can't be null\n";
        else if (sr.getRoom().getId() == null)  errors += "Invalid room: room must have id\n";
        if (sr.getMaxPeople() == null)          errors += "Invalid max_people: can't be null\n";
        else if (sr.getMaxPeople() < 1)         errors += "Invalid max_people: must allow more than 0 people\n";
        if (sr.getName() == null)               errors += "Invalid name: can't be null\n";
        if (!errors.isEmpty())
            throw new IllegalArgumentException(errors.trim());
    }

    private void validateSubRoom(SubRoom sr) throws Exception {
        validateDataFields(sr);

        sr.setRoom(roomService.get(sr.getRoom().getId()));
        if (sr.getRoom() == null) throw new NotFoundException("Invalid room: no room of given id exists");

        int sum = 0;
        for (SubRoom dbSr : sr.getRoom().getSubRooms()) if (!dbSr.equals(sr)) sum += dbSr.getMaxPeople();
        if (sum + sr.getMaxPeople() > sr.getRoom().getMaxPeople())
            throw new IllegalArgumentException("Invalid max_people: new subRoom for given room can't contain more than " + (sr.getRoom().getMaxPeople() - sum) + " people");
    }

    public boolean add(SubRoom sr) throws Exception {
        sr.setId(null);
        sr.setLeases(null);

        validateSubRoom(sr);

        return super.add(sr);
    }

    @Override
    public boolean update(int id, SubRoom sr) throws Exception {
        SubRoom dbSr = get(id);
        if (dbSr == null) throw new NotFoundException("Invalid id: no room_section has id " + id);

        sr.setId(id);
        sr.setRoom(dbSr.getRoom());

        validateSubRoom(sr);

        return super.update(id, sr);
    }
}
