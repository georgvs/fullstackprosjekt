package com.example.demo.Service;

import com.example.demo.Exception.ConflictException;
import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.Basic.BasicService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class UserService extends BasicService<User> {
    private UserRepository userRepository;
    @Autowired
    PasswordEncoder encoder;

    private static final String defaultRole = "USER";
    private static final String[] permittedRoles = { defaultRole, "ADMIN", "DEBUG" };

    public UserService(UserRepository repository) {
        super(repository);
        this.userRepository = repository;
    }

    public User getByEmail(String email) throws Exception {
        return userRepository.getByEmail(email);
    }
    public User getByName(String name) throws Exception {
        return userRepository.getByName(name);
    }
    public List<User> getByRole(String role) throws Exception {
        return userRepository.getByRole(role);
    }

    /**
     * Validates that user doesn't have overlapping unique datafiles with database instances of differing ids
     * @param u user to be added or updated
     * @throws Exception if conflicting with database instance of different id
     */
    private void validateDataFields(User u) throws Exception {
        String errors = "";
        if (u.getEmail() == null)                       errors += "Invalid email: can't be null\n";
        else {
            User dbU = getByEmail(u.getEmail());
            if (dbU != null && !dbU.equals(u))          errors += "Invalid email: " + u.getEmail() + " is taken\n";
        }
        if (u.getName() == null)                        errors += "Invalid name: can't be null\n";
        else {
            User dbU = getByName(u.getName());
            if (dbU != null && !dbU.equals(u))          errors += "Invalid name: " + u.getName() + " is taken\n";
        }
        if (u.getRole() == null)                        errors += "Invalid role: can't be null\n";
        if (u.getPassword() == null)                    errors += "Invalid password: can't be null\n";
        if (u.getPhone() == null)                       errors += "Invalid phone: can't be null\n";
        if (u.getValidUntil() == null)                  errors += "Invalid validUntil: can't be null\n";
        else if (u.getValidUntil().before(new Date()))  errors += "Invalid validUntil: can't be before current time\n";

        if (!errors.isEmpty())
            throw new ConflictException(errors.trim());
    }

    /**
     * if user has no role, role is set to defaultRole,
     * if user has non permitted role IllegalArgumentException is thrown
     * @param u user
     */
    private void normalizeRole(User u) throws IllegalArgumentException {
        String r = u.getRole();
        if (r.isEmpty()) {
            u.setRole(defaultRole);
            return;
        }
        for (String pr : permittedRoles)
            if (r.equals(pr)) return;
        throw new IllegalArgumentException("Invalid role: " + u.getRole() + " is not a valid role.\nPermitted roles are: " + Arrays.toString(permittedRoles));
    }

    @Override
    public boolean add(User u) throws Exception {
        u.setId(null);
        validateDataFields(u);
        normalizeRole(u);

        u.setPassword(encoder.encode(u.getPassword()));
        return super.add(u);
    }

    @Override
    public boolean update(int id, User u) throws Exception {
        if (get(id) == null) throw new NotFoundException("Invalid id: no user has id " + id);
        u.setId(id);
        validateDataFields(u);
        normalizeRole(u);

        u.setPassword(encoder.encode(u.getPassword()));
        return super.update(id, u);
    }
}
