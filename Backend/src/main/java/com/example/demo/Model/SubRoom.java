package com.example.demo.Model;

import com.example.demo.Model.Interface.IDable;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.Set;

@Entity
@Cacheable(false) /* avoids data corruption in database when other connections are altering SubRooms simultaneously */
public class SubRoom extends IDable {
    private String name;
    private String description;
    private Integer maxPeople;
    @ManyToOne
    @JsonBackReference
    private Room room;
    @ManyToMany(mappedBy = "subRooms")
    @JsonIgnore
    private Set<Lease> leases;

    public SubRoom() {
    }

    public SubRoom(Integer id, String name, String description, Integer maxPeople, Room room, Set<Lease> leases) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.maxPeople = maxPeople;
        this.room = room;
        this.leases = leases;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Set<Lease> getLeases() {
        return leases;
    }

    public void setLeases(Set<Lease> leases) {
        this.leases = leases;
    }
}
