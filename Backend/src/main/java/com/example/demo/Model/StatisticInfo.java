package com.example.demo.Model;

public class StatisticInfo {

    private User user;


    private long hours;

    public StatisticInfo(User user, long hours) {
        this.user = user;
        this.hours = hours;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



    public long getHours() {
        return hours;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }

}
