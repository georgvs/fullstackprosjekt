package com.example.demo.Model;

import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Web.SimpleType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
public class Equipment extends IDable implements SimpleType<Equipment> {
    private String name;
    private String type;
    private String description;
    private String serialNumber;
    @ManyToMany(mappedBy = "equipment")
    @JsonIgnore
    private Set<Lease> leases;

    public Equipment() {
    }

    public Equipment(Integer id, String name, String type, String description, String serialNumber, Set<Lease> leases) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.serialNumber = serialNumber;
        this.leases = leases;
    }

    @Override
    @JsonBackReference
    public Equipment getUnsimplified() {
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Set<Lease> getLeases() {
        return leases;
    }

    public void setLeases(Set<Lease> leases) {
        this.leases = leases;
    }
}
