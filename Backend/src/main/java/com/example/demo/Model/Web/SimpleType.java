package com.example.demo.Model.Web;

public interface SimpleType<T> {
    T getUnsimplified();
}
