package com.example.demo.Model.Web;

import com.example.demo.Model.Equipment;
import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Lease;
import com.example.demo.Model.SubRoom;
import com.example.demo.Model.User;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Lease model for web layer input (transformed into normal Lease before entering service layer)
 */
public class SimpleLease extends IDable implements SimpleType<Lease> {
    private Date from;
    private String description;
    private Date to;
    private Integer leaserId;
    private List<Integer> subRoomIds;
    private List<Integer> equipmentIds;

    public SimpleLease() {
    }
    public SimpleLease(Integer id, Date from, Date to, String description, Integer leaserId, List<Integer> subRoomIds, List<Integer> equipmentIds) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.description = description;
        this.leaserId = leaserId;
        this.subRoomIds = subRoomIds;
        this.equipmentIds = equipmentIds;
    }

    @Override
    public Lease getUnsimplified(){
        Set<SubRoom> subRooms = new HashSet<>();
        if (subRoomIds != null) for (Integer i : subRoomIds){
            if (i == null) continue;
            SubRoom sr = new SubRoom();
            sr.setId(i);
            subRooms.add(sr);
        }
        Set<Equipment> equipment = new HashSet<>();
        if (equipmentIds != null) for (Integer i : equipmentIds){
            if (i == null) continue;
            Equipment e = new Equipment();
            e.setId(i);
            equipment.add(e);
        }

        User leaser = new User();
        leaser.setId(leaserId);

        return new Lease(id, from, to, description, leaser, subRooms, equipment);
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLeaserId() {
        return leaserId;
    }

    public void setLeaserId(Integer leaserId) {
        this.leaserId = leaserId;
    }

    public List<Integer> getSubRoomIds() {
        return subRoomIds;
    }

    public void setSubRoomIds(List<Integer> subRoomIds) {
        this.subRoomIds = subRoomIds;
    }

    public List<Integer> getEquipmentIds() {
        return equipmentIds;
    }

    public void setEquipmentIds(List<Integer> equipmentIds) {
        this.equipmentIds = equipmentIds;
    }
}
