package com.example.demo.Model.Web;

import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Message;
import com.example.demo.Model.SubRoom;
import com.example.demo.Model.User;
import java.util.Date;

public class SimpleMessage extends IDable implements SimpleType<Message> {
    private String content;
    private Date postTime;
    private Integer posterId;
    private Integer subRoomId;

    public SimpleMessage() {
    }

    public SimpleMessage(Integer id, String content, Date postTime, Integer posterId, Integer subRoomId) {
        this.id = id;
        this.content = content;
        this.postTime = postTime;
        this.posterId = posterId;
        this.subRoomId = subRoomId;
    }

    @Override
    public Message getUnsimplified(){
        User poster = new User();
        poster.setId(posterId);
        SubRoom subRoom = new SubRoom();
        subRoom.setId(subRoomId);
        return new Message(id, content, postTime, poster, subRoom);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public Integer getPosterId() {
        return posterId;
    }

    public void setPosterId(Integer posterId) {
        this.posterId = posterId;
    }

    public Integer getSubRoomId() {
        return subRoomId;
    }

    public void setSubRoomId(Integer subRoomId) {
        this.subRoomId = subRoomId;
    }
}
