package com.example.demo.Model;

import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Web.SimpleType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Entity;
import java.util.Date;

@Entity
public class User extends IDable implements SimpleType<User> {
    private String name;
    private String email;
    private String phone;
    private String role;
    private String password;
    private Date validUntil;

    public User() {
    }

    public User(Integer id, String name, String email, String phone, String role, String password, Date validUntil) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.password = password;
        this.validUntil = validUntil;
    }

    @Override
    @JsonBackReference
    public User getUnsimplified() {
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }
}
