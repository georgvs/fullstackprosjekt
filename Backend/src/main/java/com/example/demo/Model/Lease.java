package com.example.demo.Model;

import com.example.demo.Model.Interface.IDable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Lease extends IDable {
    @Column(name = "_FROM")
    private Date from;
    @Column(name = "_TO")
    private Date to;
    private String description;
    @ManyToOne
    private User leaser;
    @ManyToMany
    private Set<SubRoom> subRooms;
    @ManyToMany
    private Set<Equipment> equipment;

    public Lease() {
    }
    public Lease(Integer id, Date from, Date to, String description, User leaser, Set<SubRoom> subRooms, Set<Equipment> equipment) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.description = description;
        this.leaser = leaser;
        this.subRooms = subRooms;
        this.equipment = equipment;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getLeaser() {
        return leaser;
    }

    public void setLeaser(User leaser) {
        this.leaser = leaser;
    }

    public Set<SubRoom> getSubRooms() {
        return subRooms;
    }

    public void setSubRooms(Set<SubRoom> subRooms) {
        this.subRooms = subRooms;
    }

    public Set<Equipment> getEquipment() {
        return equipment;
    }

    public void setEquipment(Set<Equipment> equipment) {
        this.equipment = equipment;
    }
}
