package com.example.demo.Model;

import com.example.demo.Model.Interface.IDable;
import com.example.demo.Model.Web.SimpleType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.*;
import java.util.Set;

@Entity
public class Room extends IDable implements SimpleType<Room> {
    private String name;
    private Integer maxPeople;
    @OneToMany(mappedBy = "room")
    @JsonManagedReference
    private Set<SubRoom> subRooms;

    public Room() {
    }

    public Room(Integer id, String name, Integer maxPeople, Set<SubRoom> subRooms) {
        this.id = id;
        this.name = name;
        this.maxPeople = maxPeople;
        this.subRooms = subRooms;
    }

    @Override
    @JsonBackReference
    public Room getUnsimplified() {
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(Integer maxPeople) {
        this.maxPeople = maxPeople;
    }

    public Set<SubRoom> getSubRooms() {
        return subRooms;
    }

    public void setSubRooms(Set<SubRoom> subRooms) {
        this.subRooms = subRooms;
    }
}
