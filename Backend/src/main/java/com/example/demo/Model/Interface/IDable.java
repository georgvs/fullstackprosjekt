package com.example.demo.Model.Interface;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class IDable {
    @Id
    @GeneratedValue
    protected Integer id;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj.getClass().equals(this.getClass())) {
            if (this.getId() == null || ((IDable) obj).getId() == null)
                return false;
            if (((IDable) obj).getId().equals(this.getId()))
                return true;
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    public Integer getId(){
        return id;
    }
    public void setId(Integer id){
        this.id = id;
    }
}
