package com.example.demo.Model;

import com.example.demo.Model.Interface.IDable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Message extends IDable {
    private String content;
    private Date postTime;
    @ManyToOne
    private User poster;
    @ManyToOne
    private SubRoom subRoom;

    public Message() {
    }

    public Message(Integer id, String content, Date postTime, User poster, SubRoom subRoom) {
        this.id = id;
        this.content = content;
        this.postTime = postTime;
        this.poster = poster;
        this.subRoom = subRoom;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public User getPoster() {
        return poster;
    }

    public void setPoster(User poster) {
        this.poster = poster;
    }

    public SubRoom getSubRoom() {
        return subRoom;
    }

    public void setSubRoom(SubRoom subRoom) {
        this.subRoom = subRoom;
    }
}
