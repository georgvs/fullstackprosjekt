package com.example.demo.Repository;

import com.example.demo.Model.SubRoom;
import com.example.demo.Repository.Basic.BasicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class SubRoomRepository extends BasicRepository<SubRoom> {
    public SubRoomRepository() {
        super(SubRoom.class);
    }
}
