package com.example.demo.Repository;

import com.example.demo.Model.Message;
import com.example.demo.Repository.Basic.BasicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class MessageRepository extends BasicRepository<Message> {
    public MessageRepository() {
        super(Message.class);
    }
}
