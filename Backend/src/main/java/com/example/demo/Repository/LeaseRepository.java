package com.example.demo.Repository;

import com.example.demo.Model.Lease;
import com.example.demo.Repository.Basic.BasicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class LeaseRepository extends BasicRepository<Lease> {
    public LeaseRepository(){
        super(Lease.class);
    }
}
