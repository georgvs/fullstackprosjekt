package com.example.demo.Repository;

import com.example.demo.Model.User;
import com.example.demo.Repository.Basic.BasicRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class UserRepository extends BasicRepository<User> {
    public UserRepository(){
        super(User.class);
    }

    public User getByEmail(String email) throws Exception {
        return getUniqueBy("email", email);
    }
    public User getByName(String name) throws Exception {
        return getUniqueBy("name", name);
    }
    public List<User> getByRole(String role) throws Exception {
        return getBy("role", role);
    }
}
