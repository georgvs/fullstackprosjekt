package com.example.demo.Repository.Basic;

import com.example.demo.Model.Interface.IDable;
import javassist.NotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class BasicRepository<T extends IDable> {
    protected static EntityManagerFactory emf;
    private Class<T> C;

    protected BasicRepository() {
        try {
            connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public BasicRepository(Class<T> C) {
        this();
        this.C = C;
    }

    /**
     * @return Class name of managed entity
     */
    public String getModelName(){
        return C.getSimpleName();
    }

    /**
     * Super class for all the repos that facilitates the connection to the mysql database
     * @throws IOException throws io exceptions
     */
    private void connect() throws IOException {
        Properties prop = new Properties();
        HashMap<String, String> newProperties = new HashMap<>();
        //loads the local .properties file
        InputStream input = getClass().getClassLoader().getResourceAsStream("application.properties");
        // load a properties file
        assert input != null;
        prop.load(input);
        input.close();

        newProperties.put("javax.persistence.jdbc.url", "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/" + prop.getProperty("URL"));
        newProperties.put("javax.persistence.jdbc.user", prop.getProperty("USERNAME"));
        newProperties.put("javax.persistence.jdbc.password", prop.getProperty("PASSWORD"));

        emf = javax.persistence.Persistence.createEntityManagerFactory("sql-db", newProperties);
    }


    /* PUBLIC METHODS: */

    /**
     * (INSERT INTO table (id, ...) VALUES ({@param o}.id , ...))
     *
     * Adds entity to database
     * @param o entity to be added
     * @return true on successful addition*
     * @throws Exception for overloading methods
     */
    public boolean add(T o) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(o);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }

    /**
     * (DELETE FROM table WHERE id = {@param o}.id)
     *
     * Removes entity from database
     * @param o entity to be deleted
     * @return true on successful deletion
     * @throws Exception for overloading methods
     */
    public boolean delete(T o) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            if (!em.contains(o)) o = em.merge(o);
            em.remove(o);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }

    /**
     * (SELECT * FROM table)
     *
     * Gets all entities
     * @return all entities of type T
     * @throws Exception for overloading methods
     */
    public List<T> get() throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();

            CriteriaQuery<T> criteria = em.getCriteriaBuilder().createQuery(C);
            criteria.from(C);
            List<T> answer = em.createQuery(criteria).getResultList();

            em.getTransaction().commit();
            return answer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }

    /**
     * (SELECT * FROM table WHERE id = {@param id})
     *
     * Gets entity with given id
     * @param id id of entity
     * @return entity with given id
     * @throws Exception for overloading methods
     */
    public T get(int id) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            T answer = em.find(C, id);
            em.getTransaction().commit();
            return answer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }

    /**
     * (SELECT * FROM table WHERE {@param parameter} = {@param value})
     *
     * Generic query transaction where {@param parameter} is the column to be searched
     * and {@param value} is the search filter
     * @param parameter column to be searched
     * @param value search filter
     * @return entities with value of {@param value} in column of {@param parameter}
     * @throws Exception for overloading methods
     */
    protected List<T> getBy(String parameter, String value) throws Exception {
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();

            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T> criteria = builder.createQuery(C);
            Root<T> root = criteria.from(C);

            criteria.select(root)
                    .where(builder.equal(root.get(parameter), value));

            List<T> answer = em.createQuery(criteria).getResultList();

            em.getTransaction().commit();

            if (answer != null && answer.isEmpty()) answer = null;
            return answer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }

    /**
     * (SELECT * FROM table WHERE {@param parameter} = {@param value})
     *
     * Uses {@link this.getBy(String, String)} and throws exception if result is not unique
     * @param parameter same as {@link this.getBy(String, String)}
     * @param value same as {@link this.getBy(String, String)}
     * @return {@link this.getBy}
     * @throws Exception NonUniqueResultException if multiple entities exist with given constraints
     */
    protected T getUniqueBy(String parameter, String value) throws Exception {
        List<T> res = getBy(parameter, value);
        if (res == null) return null;

        if (res.size() > 1) throw new NonUniqueResultException("Found " + res.size() + " instances with " + parameter + " equal to " + value);

        return res.get(0);
    }

    /**
     * (UPDATE table SET ... WHERE id = {@param id})
     *
     * Updates entity with given id to equal given entity
     * @param id of entity to be updated
     * @param o object containing new data for entity
     * @return true on successful update
     * @throws Exception NoyFoundException if entity with given id ({@param id}) doesn't exist
     */
    public boolean update(int id, T o) throws Exception {
        EntityManager em = emf.createEntityManager();

        if (this.get(id) == null) throw new NotFoundException("No " + C.getSimpleName() + " with id " + id);

        o.setId(id);

        try {
            em.getTransaction().begin();
            em.merge(o);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            em.close();
        }
    }
}
