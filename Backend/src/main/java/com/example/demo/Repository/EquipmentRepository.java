package com.example.demo.Repository;

import com.example.demo.Model.Equipment;
import com.example.demo.Repository.Basic.BasicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EquipmentRepository extends BasicRepository<Equipment> {
    public EquipmentRepository(){
        super(Equipment.class);
    }
}
