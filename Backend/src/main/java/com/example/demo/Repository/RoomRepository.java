package com.example.demo.Repository;

import com.example.demo.Model.Room;
import com.example.demo.Repository.Basic.BasicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class RoomRepository extends BasicRepository<Room> {
    public RoomRepository(){
        super(Room.class);
    }
}
