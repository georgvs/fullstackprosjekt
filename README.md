<h1>Application for leasing rooms and equipment</h1>
<br><br>

Developped by Georg Vilhelm Seip, Erik Kaasbøll Haugen and Morten Stavik Eggen

To run the project <br>
Terminal 1: <br>
cd backend<br>
mvn spring-boot:run<br>

terminal 2:<br>
cd frontend<br>
npm install<br>
npm run serve<br>

Backend:<br>
Written in java, mysql is used for database purposes. Spring boot and maven used as build systems.<br>
Backend is well structured which allows for easy expansion in the future.<br>
Runs on port 8081.
<br><br>
Frontend:<br>
Written in vue, javascript, bootstrap, html and css<br>
Simplicity was important when designing the UI.<br>
Runs on http://localhost:8080/<br>
Admin user for testing <br>
Username: admin <br>
Password: admin <br>
Normal user (unless you want to make one with the admin yourself)<br>
Username: bob <br>
Password: bob <br>
<br><br>
Future plans for backend:<br>
Add support for groups where multiple users can lease a section of a room togheter.<br>
Add more statistics.
<br><br>
Future plan for Frontend:<br>
Frontend leaves a bit to be desired.<br>
In reservations we would make it more user friendly by allowing filtering, and more information about the rooms and subrooms<br>
that you are looking to lease<br>
We also add support for calenders, where you would be able to see the dates you have booked rooms for, and as admin you could customize this.<br>
We found resources to do it but a little to late to commit to doing it. <br>
Our main focus during this project was the backend, which our product reflects.<br>
Our frontend guy (Morten) was sick for half the project and had to check for corona aswell. <br>



