import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Login',
        component: () =>
            import ('../views/Login.vue')
    },
    {
        path: '/menu',
        name: 'Menu',
        component: () =>
            import ('../views/Menu.vue')
    },
    {
        path: '/roomRes',
        name: 'Room Reservation',
        component: () =>
            import ('../views/RoomRes.vue')
    },
    {
        path: '/chat',
        name: 'Chat',
        component: () =>
            import ('../views/Chat.vue')
    },
    {
        path: '/adminController',
        name: 'Admin Controller',
        component: () =>
            import ('../views/AdminController.vue')
    },
    {
        path: '/adminBrukere',
        name: 'Admin Brukere',
        component: () =>
            import ('../views/AdminBrukere.vue')
    },
    {
        path: '/adminStatistikk',
        name: 'Admin Statistikk',
        component: () =>
            import ('../views/AdminStatistikk.vue')
    },
    {
        path: '/adminUtstyr',
        name: 'Admin Utstyr',
        component: () =>
            import ('../views/AdminUtstyr.vue')
    },
    {
        path: '/myReservations',
        name: 'My Reservations',
        component: () =>
            import ('../views/MyReservations.vue')
    },
    {
        path: '/adminRoom',
        name: 'Admin Rom',
        component: () =>
            import ('../views/AdminRoom.vue')
    },
    {
        path: '/adminSubRoom',
        name: 'Admin SubRom',
        component: () =>
            import ('../views/AdminSubRoom.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router