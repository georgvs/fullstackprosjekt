export class Lease {
overlap(start0, end0, start1, end1){
    return (start0 <= end1) && (start1 <= end0);
}

available(item, leases, from, to){
    var overlap = false;
    if (leases) leases.forEach(lease => {
        var f = subRoom =>{
            if(subRoom.id === item.id && this.overlap(new Date(lease.from), new Date(lease.to), new Date(from), new Date(to))){
                overlap = true;
            }
        };
        if (item.type) lease.equipment.forEach(f);
        else lease.subRooms.forEach(f);
    });
    return !overlap;
}
}